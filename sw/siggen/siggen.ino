#include <TimerOne.h>

// This example uses the timer interrupt to blink an LED
// and also demonstrates how to share a variable between
// the interrupt and the main program.


static const int led = LED_BUILTIN;  // the pin with a LED
static const int key = A2;
static const int keygnd = A0;

static const unsigned long freq_pulse = 1000000/3215;
static const unsigned long freq_nopulse = 1000000/3152;


void setup(void)
{
  Serial.begin(9600);
  
  pinMode(keygnd, OUTPUT);
  digitalWrite(keygnd, LOW);
  pinMode(key, INPUT_PULLUP);
  
  pinMode(led, OUTPUT);
  Timer1.initialize(freq_nopulse);
  Timer1.attachInterrupt(blinkLED); // blinkLED to run every 0.15 seconds
}

void blinkLED(void)
{
  digitalWrite(led, !digitalRead(led));
}


void loop(void)
{
  static bool iskey = false;
  bool curkey = digitalRead(key);

  if (iskey != curkey) {
    if (iskey) {
      Serial.println(F("pulse"));
      Timer1.setPeriod(freq_pulse);
    } else {
      Serial.println(F("nopulse"));
      Timer1.setPeriod(freq_nopulse);
    }
    delay(20);
    iskey = curkey;
  }
}
