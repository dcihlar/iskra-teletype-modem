# Iskra Teletype Modem

Reverse-engineered teletype modem made by Iskra.

It converted teletype signals to 3.180kHz sine wave and vice versa.
There were also other frequencies available and multiple telex channels
could be merged into a single audio channel.
